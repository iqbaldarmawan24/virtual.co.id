import React from 'react';
import { Route, Switch } from 'react-router-dom';
import DataRuangan from '../Views/DataRuangan';
import DataTransaksi from '../Views/DataTransaksi';
import Home from '../Views/Home';
import MyProfile from '../Views/MyProfile';
import Pesan from '../Views/Pesan';
import TambahRuangan from '../Views/TambahRuangan';
import TransaksiMasuk from '../Views/TransaksiMasuk';
import Ulasan from '../Views/Ulasan';

function index(props) {
    return (
        <div>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route path="/pesan">
                    <Pesan />
                </Route>
                <Route path="/transaksi-masuk">
                    <TransaksiMasuk />
                </Route>
                <Route path="/data-transaksi">
                    <DataTransaksi />
                </Route>
                <Route path="/tambah-ruangan">
                    <TambahRuangan />
                </Route>
                <Route path="/data-ruangan">
                    <DataRuangan />
                </Route>
                <Route path="/ulasan">
                    <Ulasan />
                </Route>
                <Route path="/profile">
                    <MyProfile />
                </Route>
            </Switch>
        </div>
    );
}

export default index;