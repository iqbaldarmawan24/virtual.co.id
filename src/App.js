import './App.css';
import Navbar from './Components/Navbar';
import Router from './router'

function App() {
  return (
    <div>
      <Navbar />
      <div className="content-wrapper">
        <div className="container-fluid">
          <Router />
        </div>
      </div>
    </div>
  );
}

export default App;
