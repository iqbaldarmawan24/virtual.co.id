import React from 'react';
import Breadcrumbs from '../Components/Breadcrumbs';
import Tables from '../Components/Tables';

function DataTransaksi(props) {
    return (
        <div>
            <Breadcrumbs
                title="Data Transaksi"
            />
            <Tables />
        </div>
    );
}

export default DataTransaksi;