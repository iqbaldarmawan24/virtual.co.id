import React from 'react';

function CardMessages(props) {
    return (
        <li>
            <span>2 hours ago</span>
            <figure><img src="img/avatar1.jpg" alt="" /></figure>
            <h4>Enzo Ferrari <i className="unread">Unread</i></h4>
            <p>In vim mucius menandri convenire, an brute zril vis. Ancillae delectus necessitatibus no eam, at porro solet veniam mel, ad everti nostrud vim. Eam no menandri pertinacia deterruisset.</p>
        </li>
    );
}

export default CardMessages;